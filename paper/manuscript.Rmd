---
output:
  pdf_document:
    citation_package: natbib
    keep_tex: true
    fig_caption: true
    latex_engine: xelatex
    template: ../../svm-r-markdown-templates/svm-latex-ms.tex
    pandoc_args: "--pdf-engine-opt=--shell-escape"
header-includes:
   - \usepackage{svg}
   - \usepackage{float}
   - \usepackage{wrapfig}
title: "Politically Diverse Taste Communities"
thanks: "Replication files are available on the author's GitLab account (https://gitlab.com/SeanFischer/imdb-film-polarization). **Current version**: `r format(Sys.time(), '%B %d, %Y')`; **Corresponding author**: sean.fischer@asc.upenn.edu."
author:
- name: Sean Fischer
  affiliation: The Annenberg School for Communication
abstract: "As affective polarization has increased, partisanship has become an increasingly appealing lens through which to explain a variety of apolitical differences found in society. These include decisions about where to live, what to watch, and what to wear. However, the evidence supporting these conclusions is often analyzed and presented in aggregate terms and has come to be challenged in recent work. In this study, I pair ratings from the Internet Movie Database (IMDB) with data about following behavior on Twitter to analyze whether movie preferences are associated with political orientation. I do so by constructing a network of IMDB users who are connected when they positively review the same film. From this network, I extract and analyze each individual ego network, comparing that network's average political composition and its associated variance with the central user's own political orientation. I find that there is no relationship between the political composition of these ego networks and the ego's political orientation. Similarly, I find no substantial relationship between the diversity of the political composition of the ego network's and the ego's political orientation."
keywords: "partisanship, cultural taste, taste communities, network analysis"
wordcount: "3,934 without references; 4,906 with references"
date: "`r format(Sys.time(), '%B %d, %Y')`"
geometry: margin=1in
fontfamily: mathpazo
fontsize: 12pt
spacing: single
bibliography: ../../references/fischerbib.bib
# biblio-style: apsr
endnote: no
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(ggplot2)
library(dplyr)
library(igraph)
library(patchwork)
load("../data/graph_bb_3.RData")
```

<!-- MOTIVATION -->
In an age of heightened political polarization, especially in interpersonal settings, it is appealing to use politics as the lens through which we see the world around us. Since the divisive 2016 election, a common theme in the popular coverage of lifestyle trends has been the apparent differences in cultural tastes between Democrats and Republicans [@PoliticoPartisanBrands]. It now appears, according to these accounts that Democrats and Republicans prefer different television programs [@Katz2016], books [@Piper2016a], and clothing [@WSJPartisanClothes]. The veracity of these differences has become so accepted that cultural stereotypes of each party have become widely pervasive [@Deichert2018Thesis] and are used to make the inferences necessary to employ partisan biases in situations when they otherwise would be inaccessible. But, the question remains open: do these differences actually exist materially in the lives of individuals?

<!-- RESEARCH QUESTION -->
Similar claims of partisanship influencing apolitical decision making [@Bishop2009] have been shown to be inaccurate when taking into account more precise data [@Nall2017]. Even politically-adjacent behavior, such as reading news on the Internet has been shown to be less politically polarized than many assumed [@Guess2018]. As such, we should ask: *is partisanship really influencing cultural taste?* The answer to this question will help us to develop a better sense of the firmness of the structure underlying our partisan stereotypes. It is entirely possible that the belief in strong partisan cultural divergence is a misperception similar to others shared by partisans [@Ahler2018].

<!-- MAIN CONTRIBUTION -->
To answer this question in this study, I analyze empirical audience data to test for any link between individuals' political orientation and their cultural taste using a network analytic approach. In doing so, I provide the first individual-level results drawn from actual audience behavior data in regards to such a link. Prior studies have presented aggregate-level analyses showing mixed evidence for any partisan link between partisanship and cultural taste [@Piper2016a; @Katz2016]. Employing my approach, I am able to make specific conclusions about the individual-level political composition of what Netflix calls *taste communities*, the groups of audience members defined by their shared preferences for cultural and entertainment content.

<!-- METHOD -->
I am able to describe the individual-level composition of these taste communities by employing a network analytic approach to studying media audiences to data linking Internet Movie Database (IMDB) film ratings to following behavior on Twitter. This network-based method, developed in the study of web traffic [@Ksiazek2011; @Webster2012; @Mukerjee2018; @Majo2019], links users to the media products they consume, or in a small modification, those that they show a preference for. From this original two-mode network of consumers and products, I project the one-mode network of film consumers linked together by their shared movie preferences. By analyzing the following decisions of these IMDB users I am also able to infer their political ideology [@Barbera2015c] and add it as data to the network. From there, I study each user's individual taste community, defined as their immediate set of neighbors in the network, measuring the group's average political orientation, as well as the variance in the group's political orientation.

<!-- FINDINGS -->
From this approach, I find no evidence of a link between political orientation and film preferences. Instead, my results show that the audience members with whom an individual shares their film preferences represent a diverse set of political leanings unassociated with the individual's own political preferences. Similarly, there is no substantial relationship betwen one's own political orientation and the variance in their neighbors' political orientation. Such results leave little room for a conclusion other than that there is no meaningful link between cultural taste and political orientation.

<!-- PAPER ROADMAP -->

# Background and Predictions
Partisanship in the United States has become an increasingly relevant and highly studied aspect of political communication since the introduction of affective polarization as a framework for describing partisan animosity [@Iyengar2012]. In the years since the initial study by Iyengar, Sood, and Lelkes, scholars have articulated social-psychological theories about how partisanship has come to organize other social identities, leading to a spillover of politicization into everyday life [@Mason2018]. Such spillovers have resulted in measurable animosity on par with racial animosity [@Iyengar2015].

Notably, partisanship appears to be influencing decision making in a variety of otherwise apolitical contexts. For example, partisanship appears to influence with whom individuals form romantic relationships [@Huber2017; @Iyengar2018], as well as with whom individuals would prefer to work [@McConnell2018]. Partisanship may even affect where people choose to live [@Bishop2009; @TamCho2013]. The underlying mechanisms at work allowing partisanship to become so influential are often described as either *partisan homophily* or *partisan discrimination*. The former accounts for contexts in which individuals wish to join with and support other in-party members, while the latter describes contexts in which individuals wish to punish and distance themselves from out-party members.

The acceptance of partisanship's wide-ranging influence has since led to the emergence of popular narratives of partisan cultural divergence. Claims of this type are generally structured around the presentation of aggregate level preference or choice data that appear to show that partisans prefer different television programs [@Katz2016], books [@Piper2016a], and clothes [@WSJPartisanClothes], to name just a few examples. Complementing these popular accounts in the mass media are academic studies showing that such aggregate group-level differences may actually exist in regards to our cultural and entertainment tastes [@Shi2017; @Bertrand2018; @Mutz2018]. These results are particularly credible because social-psychology and consumer-behavior research has found that individuals' consumption decisions are influenced by the need to both signal in-group affiliation, while also signaling non-membership in opposed groups [@Berger2008; @Chan2012; @Brewer1991].

This alignment of cultural taste with partisanship is a particularly important aspect of contemporary partisanship because of how such a relationship facilitates further affective polarization. Partisans now broadly share a set of cultural stereotypes about members of the two parties and use these stereotypes to infer the partisan leanings of strangers [@Deichert2018Thesis; @HiaeshutterRice2019; @Hetherington2018]. In turn, these inferences enable individuals to employ partisan biases in interpersonal situations that would otherwise be devoid of partisan conflict [@Deichert2018Thesis]. As a result, these situations exacerbate partisan tensions and worsen the conflictual nature of contemporary political life.

However, the evidence underlying the notion of partisanship as a decision-making heuristic is still widely debated. Partisanship has been shown to be a factor in hypothetical evaluations of locations to move to, but is often superseded by other concerns when individuals actually move [@Nall2017]. Even in the case of news-media consumption, a behavior frequently associated with partisanship [@Iyengar2009; @Stroud2008; @Garrett2009b], new evidence suggests that partisan differences are relatively small [@Guess2018; @Gentzkow2011; @Eady2019; @Barbera2015c]. If partisanship is not as influential in everyday life as many assume, then partisan cultural stereotypes may be grounded in misperceptions of the other side, which are common [@Ahler2018].

As such, this scholarly conflict over the influence of partisanship leaves open the question: *does one's partisanship or political orientation influence their cultural taste*? In this study, I answer the question by analyzing an audience of film viewers who use the Internet Movie Database (IMDB) to keep track of their film ratings. Of course, if the answer to this question is yes, how would we know? In this case, if partisanship or political orienation is influencing audience members cultural taste, we would expect them to share film preferences -- or membership in what Netflix calls *taste communities* -- with others with similar political leanings (Hypothesis 1). However, the alternative hypothesis, here, is that political orientation has no empirical effect on the formation of taste communities (Hypothesis 2). Additionally, if ideology is related to cultural taste we should expect those at the ideological extremes to belong to the most politically homogenous taste communities (Hypothesis 3). Alternatively, if ideology is not related to cultural taste, then we should find that there is no relationship between the political diversity of one's taste community and the extremity of one's political orientation (Hypothesis 4).

# Data and Method
The data analyzed in this study were originally collected as part of the Movie Tweetings project [@MovieTweetings]. The Movie Tweetings project began as a way to improve the available datasets for research on recommendation systems. The project involves the perpetual scraping of tweets generated through IMDB when users decide to share the rating they have given to a film. 

When I downloaded the Movie Tweetings database, it contained 819,845 ratings from 60,658 users. These ratings covered 37,051 movies. The ratings themselves are recorded directly from the IMDB 10-point scale. Importantly, though, the ratings also include the Twitter id for each reviewer. This data allows us to associate other profile information with each set of reviews. For this study, I scraped the list of accounts being followed by each reviewer with at least 100 scraped ratings. This threshold left only 1,642 users for whom I collected data. I then fed the lists of accounts being followed into Barbera's method for estimating political ideology from the set of elites an account follows on Twitter [@Barbera2015c]. 579 of the users had insufficient data to generate an ideology estimate and were dropped from further analysis. This left 1,063 users in the final set.

This list of users was then applied as a filter to the ratings data. Additionally, the ratings data were manipulated to create a new binary indicator variable, equal to one whenever a rating was greater than 5 on IMDB's 10 point scale. This variable captured instances in which users signaled a positive reaction to a film and was used as another filter so that ratings were only preserved when this indicator was equal to one, leaving 209,873 ratings.

These ratings are inherently relational, mapping links between users and films as a bipartite network. In this case, the network edges represent instances in which users, the first class of nodes, reacted positively to films, the second class of nodes. From this bipartite network of users and films, I performed a further manipulation by generating the one-mode projection representing the user-to-user network. In the user-to-user network, each node represents one of the 1,063 users allowed through our earlier filter and the directed edges connecting them mark instances in which two users both reacted positively to the same film. These edges have a weight equal to the number of films the given pair of reviewers both rated positively. This network generating process is visualized in **Figure 1**.

As a final threshold, I applied a disparity filter to this weighted directed network of users. The disparity filter [@Serano2009] is a statistical test for each edge in a weighted network, given that some shared preferences could happen by chance alone, and not shared taste, as the number of ratings one provides increases. This statistical test differs from some other network thresholding techniques [@Mukerjee2018] in that it establishes the null model the observed results are compared against for each ego network, not every linked pair of nodes. This difference is crucial as it determines significant edges in a network based on information from all of the edges associated with a given node. The null model in this technique assumes that edge weights are drawn from a uniform distribution across the interval [0, 1], so that each edge has the same weight. Edges whose normalized weight (so that the weights fall on the [0, 1] interval) is above a critical value determined by the threshold set for significance. In this study, that value, $p$, is set to 0.05, the traditional statistical threshold for significance. This means that each tie that is preserved occurs with low probability if edge weights are assigned randomly. Applying the disparity filter, reduced the number of edges to 18,160, down from 549,309 edges in the original network.

```{r fig1, echo = FALSE, fig.cap = "A schematic of the network generating process. ", fig.pos='t', out.extra = '', eval = TRUE, out.width="100%", fig.align="center"}
knitr::include_graphics("../figures/schematic.png")
```

<!-- The ratings data are by their very nature relational: they map the relationships between individuals and the films they watch and review. At this level, the data represent a bipartite network of films and users, but can be easily converted to a one-mode network representing film-to-film or user-to-user relationships. Projecting to single mode networks is advantageous because it allows us to assess the relationship between the properties of films or users on either their shared consumption or habits. -->

The decision to analyze the user-to-user network marks an important point of departure from prior network-based studies of audience behavior and fragmentation. These earlier studies, due to limitations imposed by data sources, were forced into studying the outlet-to-outlet or product-to-product network that is the other possible one-mode projection from this type of bipartite network [@Webster2012; @Mukerjee2018]. These studies have focused on audience fragmentation, but have been forced to study such behavior through the structure of the outlet-to-outlet or product-to-product networks projected from the user-media bipartite network. As a result, any analyses of audience characteristics have been done with product-level aggregates, increasing the uncertainty of their results.

Because I am analyzing the user-to-user network, I can conduct my analyses without having to generate average values for node-level features, which can introduce additional levels of uncertainty. Instead, I am able to assess the relationship between network structure and the specific estimate of political ideology returned from Barbera's method for each user in the network. In particular, I focus my analysis on each user's ego network. An ego network is the subgraph of a network generated by a given node and all other nodes with whom it shares an edge. The central node in an ego network is commonly referred to as *ego*. Each ego network can be described by its average ideology, operationalized here as the average Twitter ideology score of ego's neighbors, weighted by the associated weight of the edge running from ego to each neighbor, as well as its diversity of Twitter ideology scores, operationalized as the variance in the ideology scores of ego's neighbors, weighted again by the weight of the edge from ego to each neighbor. This process is visualized in **Figure 2**. To test the hypothesis that political orientation is associated with cultural taste, I visualize and model the relationship between these two measures and the estimated ideology score of each ego.

```{r fig2, echo = FALSE, fig.cap = "A schematic of the network analytic method. ", fig.pos='t', out.extra = '', eval = TRUE, out.width="100%", fig.align="center"}
knitr::include_graphics("../figures/method_schematic.png")
```

# Results
The initial bipartite network made up of 819,845 edges between 60,658 IMDB users and 37,051 films was first thresholded so as to keep only the network of users with at least 100 captured reviews. Furthermore, to take taste into account, only reviews greater than a 5 were preserved. This left 209,873 edges in the bipartite network between 1,063 users and 20,507 films. The initial one-mode projection of the user-to-user network consisted of 549,309 undirected edges between the 1,063 users.

To further preserve only those edges that we would not expect to occur from random chance, I performed backbone extraction on the network by applying a disparity filter [@Serrano2009; @Majo2019]. This filter applied a statistical test to each tie in each ego network contained within the larger whole. Those edges that could have occurred by random chance were subsequently removed. This process left 18,160 edges between the 1,063 users.

We can interpret the edges in this network as marking instances in which two users, represented as linked nodes, both positively rated at least one of the same films. The weights given to edges, assuming they link User A and User B, were weighted to indicate the number of the movies preferred by User A which were also preferred by User B. These weights range between 5 and 499, with a median of 55 and a mean of 64.81. That these edges represent connections based on actual taste preferences, we can consider the network structure to be a map of shared cultural taste and taste communities.
<!-- The distributions of edge weights is visualized in **Figure 3**.  -->

```{r fig3, echo = FALSE, eval=FALSE}
# load("../data/user_ratings_2.RData")

edge_df <- as.data.frame(get.edgelist(graph_bb))

edge_df$weight <- E(graph_bb)$weight

# edge_df <-
#   edge_df %>%
#   full_join(user_ratings_2, by = c("V1" = "userID")) %>%
#   mutate(V1_N = N,
#          normalized_weight = weight / V1_N) %>%
#   select(-N)

p_edge_1 <-
  ggplot(edge_df, aes(x = weight)) +
  geom_density() +
  xlab("Edge Weight") +
  ylab("Density") +
  # ggtitle("A.") +
  theme_bw() +
  # fischeR::theme_saf() +
  theme(legend.position = "none",
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank())

# p_edge_2 <-
#   ggplot(edge_df, aes(x = normalized_weight)) +
#   geom_density() +
#   xlab("Normalized Edge Weight") +
#   ylab("Density") +
#   ggtitle("B.") +
#   theme_bw() +
#   # fischeR::theme_saf() +
#   theme(legend.position = "none",
#         panel.grid.major = element_blank(),
#         panel.grid.minor = element_blank())

p_edge_1
```

Additionally, each user node in this network also has three measured properties: the number of positive ratings they generated, the number of neighbors they were connected to, and their one-dimensional ideology score estimated based on the elites they follow on Twitter, with low values indicating more liberal or Democratic positions and higher scores indicating more conservative or Republican positions. The distributions for the number of positive ratings and number of neighbors are predicatably heavy-tailed. The number of positive ratings ranges from 33 to 1,968, with a median of 148 and a mean of 197.4. The number of neighbors a node has ranges from 2 to 1,045, with a median of 17 and a mean of 34.17. 
<!-- , as seen in Panels A and B of **Figure 4**.  -->

However, these 1,063 users have a more symmetric and representive distribution of political leanings, based on the one-dimensional score generated from Barbera's method of analyzing the elite accounts each user follows on Twitter [@Barbera2015c]. The distribution of one-dimensional ideology scores is wide ranging, running from -2.37 to 2.41, with a median (0.19) and mode (.38) to the right of zero , but a mean value very nearly zero (0.083). This distribution suggests that this sample audience should be sufficient for identifying links between partisanship and cultural taste because it incorporates individuals across a wide swath of the American ideological landscape. There are enough conservatives in the group that if they are to be connected to anyone, they do not have to be connected to liberals and vice versa.
<!-- As we can see in Panel C of **Figure 4**, -->

```{r fig4, echo = FALSE, fig.cap = "A. The density plot for the number of positive ratings from each user. B. The density plot for the number of neighbors each user has in the user-to-user network. C. The density plot for the one-dimensional ideology scores estimated based on individuals' following decisions on Twitter. The dashed gray line marks the mean value.", fig.pos='t', out.extra = '', eval = TRUE, message=FALSE, warning=FALSE, eval=FALSE}
load("../data/graph_bb_3.RData")
load("../data/user_ratings_2.RData")

V(graph_bb)$n_neighbors <- ego_size(graph_bb, order = 1, mode = "all") - 1
V(graph_bb)$deg <- degree(graph_bb, mode = "all")

density_df <- data.frame(userID = V(graph_bb)$name,
                         twitter_ideology = V(graph_bb)$twitter_ideology)

density_df <- density(density_df$twitter_ideology, n = 2 ^ 12)

density_df <-
  data.frame(
    x = density_df$x,
    y = density_df$y
  )

p1 <- 
  ggplot(
    density_df, 
    aes(x = x, y = y)) +
  # geom_line() +
  geom_segment(aes(xend = x,
                   yend = 0, 
                   colour = x)) +
  geom_vline(xintercept = mean(density_df$x), lty = 2, color = "grey35") +
  scale_colour_gradient(low = "#5522ee", high = "#d53737") +
  xlab("Twitter Ideology") +
  ylab("Density") +
  ggtitle("C.") +
  theme_bw() +
  # fischeR::theme_saf() +
  theme(legend.position = "none",
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank())

out_df <- 
  data.frame(
    userID = V(graph_bb)$name,
    n_neighbors = V(graph_bb)$n_neighbors,
    deg = V(graph_bb)$deg
  )

out_df <-
  out_df %>%
  inner_join(user_ratings_2)

p2 <-
  ggplot(out_df, aes(x = N)) +
  geom_density() +
  xlab("Number of Ratings") +
  ylab("Density") +
  ggtitle("A.") +
  theme_bw() +
  # fischeR::theme_saf() +
  theme(legend.position = "none",
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank())

p3 <-
  ggplot(out_df, aes(x = n_neighbors)) +
  geom_density() +
  xlab("Number of Neighbors") +
  ylab("Density") +
  ggtitle("B.") +
  theme_bw() +
  # fischeR::theme_saf() +
  theme(legend.position = "none",
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank())

(p2 + p3) / p1

```

To test whether ideology is associated with this structure, I extracted and analyzed the ego-network for each user in the network. The ego network is the subgraph consisting of a given node and each node connected directly to it and is analogous to a small taste community. I then took the mean of the neighbor's one-direction ideology scores, weighting by the weight of the edge running from ego to each neighbor. If this average is close to the ideology score inferred for the ego, then that would suggest that ideology and cultural taste are linked, lending credence to Hypothesis 1 and undermining Hypothesis 2.

```{r fig5, echo = FALSE, fig.cap = "A. Relationship between user ideology score and the weighted average of their neighbors. Curve generated by local regression (loess) appears in grey. B. Relationship between us er ideology score and the weighted variance in ideology score of their neighbors. Curve generated by local regression (loess) appears in grey.", fig.pos='t', out.extra = '', eval = TRUE, cache = FALSE, warning=FALSE, message=FALSE}
out_en <- data.frame(start = V(graph_bb)$name,
                     pid_ave = V(graph_bb)$twitter_ideology,
                     n_nodes = NA,
                     ego_pid_ave = NA,
                     ego_pid_var = NA,
                     ego_diff = NA)
test_g <- simplify(graph_bb, remove.loops = TRUE, 
                   remove.multiple = FALSE)

for (i in 1:length(V(graph_bb))) {
  n_nodes <- ego_size(test_g, order = 1, nodes = i) - 1
  pid_ave <- out_en$pid_ave[i]
  ego_sub <- make_ego_graph(test_g, 
                            order = 1,
                            nodes = i)[[1]]
  list_of_vertices <- V(ego_sub)$name == V(test_g)$name[i]
  list_of_edges <- E(ego_sub)[from(list_of_vertices)]
  ego_sub <- subgraph.edges(ego_sub, list_of_edges)
  ego_df <- data.frame(
    twitter_ideology =
      V(test_g)$twitter_ideology[which(V(ego_sub)$name != V(test_g)$name[i])],
    weight = E(ego_sub)$weight
  )
  ego_pid_ave <- weighted.mean(ego_df$twitter_ideology,
                               w = ego_df$weight)
  ego_pid_var <- Hmisc::wtd.var(ego_df$twitter_ideology,
                                    weights = ego_df$weight)
  ego_diff <- pid_ave - ego_pid_ave
  out_en$n_nodes[i] <- n_nodes
  out_en$ego_pid_ave[i] <- ego_pid_ave
  out_en$ego_pid_var[i] <- ego_pid_var
  out_en$ego_diff[i] <- ego_diff
}

out_en$abs_ego_diff <- abs(out_en$ego_diff)

p_ego_1 <- 
  ggplot(out_en, aes(x = pid_ave, y = ego_pid_ave)) +
  geom_point(aes(color = pid_ave)) +
  geom_smooth(color = "grey35") +
  scale_color_gradient(low = "#4d85f5", high = "#f54d4d") +
  guides(color = FALSE) +
  xlab("Twitter Ideology of User") +
  ylab("Average Twitter Ideology of Ego Network") +
  # fischeR::theme_saf() +
  theme_bw() +
  theme(panel.grid.minor = element_blank(),
        panel.grid = element_blank())

p_ego_4 <-
  ggplot(out_en, aes(x = pid_ave, y = ego_pid_var)) +
  geom_point(aes(color = pid_ave)) +
  geom_smooth(color = "grey35") +
  scale_color_gradient(low = "#4d85f5", high = "#f54d4d") +
  guides(color = FALSE) +
  # ylim(c(0, 4)) +
  xlab("Twitter Ideology of User") +
  ylab("Variance in Twitter Ideology of Ego Network") +
  # fischeR::theme_saf() +
  theme_bw() +
  theme(panel.grid.minor = element_blank(),
        panel.grid = element_blank())

p_ego_2 <- ggplot(out_en, aes(x = pid_ave, y = abs_ego_diff)) +
  geom_point(aes(color = pid_ave)) +
  geom_smooth(color = "grey35") +
  scale_color_gradient(low = "#4d85f5", high = "#f54d4d") +
  guides(color = FALSE) +
  xlab("Average Twitter Ideology of User") +
  ylab("Absolute Difference between Ego Network and User") +
  # fischeR::theme_saf() +
  theme_bw() +
  theme(panel.grid.minor = element_blank(),
        panel.grid = element_blank())

p_ego_1 / p_ego_4
```

```{r reg1, echo = FALSE, results='asis'}
m2 <- lm(ego_pid_ave ~ pid_ave, data = out_en)
m2_2 <- lm(ego_pid_ave ~ pid_ave + I(pid_ave ^ 2), data = out_en)
m5 <- lm(ego_pid_ave ~ pid_ave + I(pid_ave ^ 2) + I(pid_ave ^ 3), data = out_en)
m3 <- lm(abs_ego_diff ~ pid_ave, data = out_en)
m4 <- lm(abs_ego_diff ~ pid_ave + I(pid_ave ^ 2), data = out_en)

stargazer::stargazer(m2, m2_2, m5,
                     covariate.labels = c("User Ideology", 
                                          "User Ideology$^{2}$",
                                          "User Ideology$^{3}$"),
                     dep.var.labels = "Mean Ego Network Ideology",
                     title = "Regression results modeling ego network average ideology on user ideology.",
                     header = FALSE,
                     float = TRUE,
                     table.placement = "t")
```

```{r, echo = FALSE,  results='asis'}
m0 <- lm(ego_pid_var ~ pid_ave, data = out_en[which(out_en$ego_pid_var > 0), ])
m1 <- lm(ego_pid_var ~ pid_ave + I(pid_ave ^ 2),
         data = out_en[which(out_en$ego_pid_var > 0), ])
stargazer::stargazer(m0, m1, 
                     covariate.labels = c("User Ideology", 
                                          "User Ideology$^{2}$"),
                     dep.var.labels = "Var. of Ego Network Ideology",
                     title = "Regression results modeling ego network variance on user ideology.",
                     header = FALSE,
                     float = TRUE,
                     table.placement = "t")
```

However, I find the opposite. As we can see in Panel A of **Figure 5**, the relationship between the inferred ideology each user and the average ideology of their neighbors in the network is basically zero; the overlaid loess curve shows very little movement across the range of values. The network assortativity, a correlation-like network measure, in regards to ideology confirms the result: it is only 0.034 on a scale ranging from -1 to 1. Furthermore, we can see in Column 1 of **Table 1** that a linear relationship between a user's political orientation and that of their neighbors, which is predicted by Hypothesis 1, is not present, undermining the theory and lending credibility to Hypothesis 2. Columns 2 and 3 of **Table 1** further show that even if we ignore the visualization and try to force a quadratic or cubic relationship between the ego network average and the associated ego's ideology score, there is still no statistically significant relationship.
<!-- We can see too in Panel B of **Figure 4** that the absolute difference between the neighborhood average and a user's ideology score is minimized as the user's inferred score approaches zero. Individuals with extreme scores at either ideological pole experience the largest difference between themselves and the network directly connected to them. -->

Additionally, as stated in Hypothesis 3, if ideology is related to cultural taste we should expect those at the ideological extremes to belong to the most politically homogenous taste communities. That is the relationship between user ideology and the variance in the ideology scores in each ego network should be concave. As a test of this hypothesis, I calculated the variance in the one-dimensional ideology scores for each user's neighbors, again weighted by the weight of the edge from the user to each neighbor. The relationship between user ideology and the variance in the ideology of their neighbors is plotted in Panel B of **Figure 5**. We can see from the overlayed loess curve that the relationship is, if anything other than flat, convex, with higher values at the tails than at the center. To confirm this observation, I regressed the variance in the neighbors' ideology scores on models including user ideology scores as just a linear term and as both the linear and squared terms, with full reults reported in **Table 2**. I find that in the second model, the quadratic term is positive ($\beta = 0.023$), but not statistically significant. This lack of significant effects, indicates that there is no relationship between one's own political orientation and the homogeneity in the political orientation of those with whom one shares film preferences, violating our expectations for what should happen if ideology and cultural taste are related, as stated in Hypothesis 3. Therefore, it appears that Hypothesis 4 is the more credible of the pair.

# Discussion
In this study, I set out to provide a strong individual-level empirical test of the claim that American partisans are experiencing cultural divergence. These claims, amplified by the popular press and supported by academics presenting aggregated results at the group level. Here, I've paired IMDB film ratings from 1,063 users with data about which elites these individuals follow on Twitter, transformed into a one-dimensional score correlated with political ideology and political orientation. I have used these paired data to construct a network linking users together when they both provide a positive review of the same film. By extracting the local neighborhood of each user in the network, I am able to compare the estimated ideology of each user with those who most share their taste profile. This approach also allows me to test whether the diversity of political orientations among one's closest taste neighbors varried less as one's political orientation grows more extreme. I find no meaningful relationships in either case, undermining the argument that political orientation is associated with cultural taste.

These results fit nicely into the recent literature arguing against partisanship's influence in other apolitical settings, such as making decisions about where to live [@Nall2017]. While this study does not incorporate any tests of any specific mechanisms, we can easily imagine that cultural taste functions similarly to taste in where to live: it is likely subject to many competing interests or desires. These desires or interests may occasionally align with those related to group identification and differentiation detailed in Optimal Distinctiveness Theory (ODT) [@Brewer1991; @Berger2007; @Berger2008], but may not be activated unless group-identification is primed. We know that in specific political contexts, social settings play an important role in how partisanship influences reasoning [@Klar2014]. Further research on the influence of partisanship in apolitical settings should focus on whether or not partisanship's influence on cultural taste is also dependent on social context.

Additionally, further research could benefit from attempting to better map the many influences on cultural taste left unmeasured in this study. Properly taking these factors into account may provide a pathway for partisanship to exert an influence on taste and, in turn, begin to make precise causal arguments possible. Yet, such a mediated or moderated causal path diverges from the spirit of the bold claims made in the popular press about the substantial fissure developing between Democrats and Republicans. As such, the analyses and results presented in this study are appropriate for testing and refuting this strong claim. The result that individuals are located within taste communities that are politically diverse, regardless of how extreme one's own political orienatation appears to be, suggests that political homogeneity is not a feature of taste communities.

\newpage
# References