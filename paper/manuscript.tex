\documentclass[12pt,]{article}
\usepackage[left=1in,top=1in,right=1in,bottom=1in]{geometry}
\usepackage{booktabs}
\usepackage{makecell}
\usepackage{rotating}
\usepackage{graphicx}
\usepackage[x11names,dvipsnames,table]{xcolor}
\usepackage{colortbl}
\newcommand*{\authorfont}{\fontfamily{phv}\selectfont}
\usepackage[]{mathpazo}


  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}


\usepackage{textcomp}
\usepackage{abstract}
\renewcommand{\abstractname}{}    % clear the title
\renewcommand{\absnamepos}{empty} % originally center

\renewenvironment{abstract}
 {{%
    \setlength{\leftmargin}{0mm}
    \setlength{\rightmargin}{\leftmargin}%
  }%
  \relax}
 {\endlist}

\makeatletter
\def\@maketitle{%
  \newpage
%  \null
%  \vskip 2em%
%  \begin{center}%
  \let \footnote \thanks
    {\fontsize{18}{20}\selectfont\raggedright  \setlength{\parindent}{0pt} \@title \par}%
}
%\fi
\makeatother




\setcounter{secnumdepth}{0}



\title{Politically Diverse Taste Communities \thanks{Replication files are available on the author's GitLab account
(\url{https://gitlab.com/SeanFischer/imdb-film-polarization}).
\textbf{Current version}: January 12, 2020; \textbf{Corresponding
author}:
\href{mailto:sean.fischer@asc.upenn.edu}{\nolinkurl{sean.fischer@asc.upenn.edu}}.}  }



\author{\Large Sean Fischer\vspace{0.05in} \newline\normalsize\emph{The Annenberg School for Communication}  }


\date{}

\usepackage{titlesec}

\titleformat*{\section}{\normalsize\bfseries}
\titleformat*{\subsection}{\normalsize\itshape}
\titleformat*{\subsubsection}{\normalsize\itshape}
\titleformat*{\paragraph}{\normalsize\itshape}
\titleformat*{\subparagraph}{\normalsize\itshape}


\bibliographystyle{apacite}
\usepackage[natbibapa]{apacite}
\usepackage[strings]{underscore} % protect underscores in most circumstances
\usepackage[hyphens,spaces]{url}
\usepackage[colorlinks,allcolors=blue]{hyperref}



\newtheorem{hypothesis}{Hypothesis}
\usepackage{setspace}

\makeatletter
\@ifpackageloaded{hyperref}{}{%
\ifxetex
  \PassOptionsToPackage{hyphens}{url}\usepackage[setpagesize=false, % page size defined by xetex
              unicode=false, % unicode breaks when used with xetex
              xetex]{hyperref}
\else
  \PassOptionsToPackage{hyphens}{url}\usepackage[unicode=true]{hyperref}
\fi
}

\@ifpackageloaded{color}{
    \PassOptionsToPackage{usenames,dvipsnames}{color}
}{%
    \usepackage[usenames,dvipsnames]{color}
}
\makeatother
\hypersetup{breaklinks=true,
            bookmarks=true,
            pdfauthor={Sean Fischer (The Annenberg School for Communication)},
            pdfkeywords = {partisanship, cultural taste, taste communities, network analysis},  
            pdftitle={Politically Diverse Taste Communities},
            colorlinks=true,
            citecolor=blue,
            urlcolor=blue,
            linkcolor=magenta,
            pdfborder={0 0 0}}
\urlstyle{same}  % don't use monospace font for urls

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother

\usepackage{svg}
\usepackage{float}
\usepackage{wrapfig}


% add tightlist ----------
\providecommand{\tightlist}{%
\setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}

\begin{document}
	
% \pagenumbering{arabic}% resets `page` counter to 1 
%
% \maketitle

{% \usefont{T1}{pnc}{m}{n}
\setlength{\parindent}{0pt}
\thispagestyle{plain}
{\fontsize{18}{20}\selectfont\raggedright 
\maketitle  % title \par  

}

{
   \vskip 13.5pt\relax \normalsize\fontsize{11}{12} 
\textbf{\authorfont Sean Fischer} \hskip 15pt \emph{\small The Annenberg School for Communication}   

}

}








\begin{abstract}

    \hbox{\vrule height .2pt width 39.14pc}

    \vskip 8.5pt % \small 

\noindent As affective polarization has increased, partisanship has become an
increasingly appealing lens through which to explain a variety of
apolitical differences found in society. These include decisions about
where to live, what to watch, and what to wear. However, the evidence
supporting these conclusions is often analyzed and presented in
aggregate terms and has come to be challenged in recent work. In this
study, I pair ratings from the Internet Movie Database (IMDB) with data
about following behavior on Twitter to analyze whether movie preferences
are associated with political orientation. I do so by constructing a
network of IMDB users who are connected when they positively review the
same film. From this network, I extract and analyze each individual ego
network, comparing that network's average political composition and its
associated variance with the central user's own political orientation. I
find that there is no relationship between the political composition of
these ego networks and the ego's political orientation. Similarly, I
find no substantial relationship between the diversity of the political
composition of the ego network's and the ego's political orientation.


\vskip 8.5pt \noindent \emph{Keywords}: partisanship, cultural taste, taste communities, network analysis \par



\vskip 8.5pt \noindent \emph{Wordcount}: 3,934 without references; 4,906 with references \par


\hbox{\vrule height .2pt width 39.14pc}

\end{abstract}


\vskip 6.5pt


\noindent \singlespacing In an age of heightened political polarization, especially in
interpersonal settings, it is appealing to use politics as the lens
through which we see the world around us. Since the divisive 2016
election, a common theme in the popular coverage of lifestyle trends has
been the apparent differences in cultural tastes between Democrats and
Republicans \citep{PoliticoPartisanBrands}. It now appears, according to
these accounts that Democrats and Republicans prefer different
television programs \citep{Katz2016}, books \citep{Piper2016a}, and
clothing \citep{WSJPartisanClothes}. The veracity of these differences
has become so accepted that cultural stereotypes of each party have
become widely pervasive \citep{Deichert2018Thesis} and are used to make
the inferences necessary to employ partisan biases in situations when
they otherwise would be inaccessible. But, the question remains open: do
these differences actually exist materially in the lives of individuals?

Similar claims of partisanship influencing apolitical decision making
\citep{Bishop2009} have been shown to be inaccurate when taking into
account more precise data \citep{Nall2017}. Even politically-adjacent
behavior, such as reading news on the Internet has been shown to be less
politically polarized than many assumed \citep{Guess2018}. As such, we
should ask: \emph{is partisanship really influencing cultural taste?}
The answer to this question will help us to develop a better sense of
the firmness of the structure underlying our partisan stereotypes. It is
entirely possible that the belief in strong partisan cultural divergence
is a misperception similar to others shared by partisans
\citep{Ahler2018}.

To answer this question in this study, I analyze empirical audience data
to test for any link between individuals' political orientation and
their cultural taste using a network analytic approach. In doing so, I
provide the first individual-level results drawn from actual audience
behavior data in regards to such a link. Prior studies have presented
aggregate-level analyses showing mixed evidence for any partisan link
between partisanship and cultural taste \citep{Piper2016a, Katz2016}.
Employing my approach, I am able to make specific conclusions about the
individual-level political composition of what Netflix calls \emph{taste
communities}, the groups of audience members defined by their shared
preferences for cultural and entertainment content.

I am able to describe the individual-level composition of these taste
communities by employing a network analytic approach to studying media
audiences to data linking Internet Movie Database (IMDB) film ratings to
following behavior on Twitter. This network-based method, developed in
the study of web traffic
\citep{Ksiazek2011, Webster2012, Mukerjee2018, Majo2019}, links users to
the media products they consume, or in a small modification, those that
they show a preference for. From this original two-mode network of
consumers and products, I project the one-mode network of film consumers
linked together by their shared movie preferences. By analyzing the
following decisions of these IMDB users I am also able to infer their
political ideology \citep{Barbera2015c} and add it as data to the
network. From there, I study each user's individual taste community,
defined as their immediate set of neighbors in the network, measuring
the group's average political orientation, as well as the variance in
the group's political orientation.

From this approach, I find no evidence of a link between political
orientation and film preferences. Instead, my results show that the
audience members with whom an individual shares their film preferences
represent a diverse set of political leanings unassociated with the
individual's own political preferences. Similarly, there is no
substantial relationship betwen one's own political orientation and the
variance in their neighbors' political orientation. Such results leave
little room for a conclusion other than that there is no meaningful link
between cultural taste and political orientation.

\hypertarget{background-and-predictions}{%
\section{Background and Predictions}\label{background-and-predictions}}

Partisanship in the United States has become an increasingly relevant
and highly studied aspect of political communication since the
introduction of affective polarization as a framework for describing
partisan animosity \citep{Iyengar2012}. In the years since the initial
study by Iyengar, Sood, and Lelkes, scholars have articulated
social-psychological theories about how partisanship has come to
organize other social identities, leading to a spillover of
politicization into everyday life \citep{Mason2018}. Such spillovers
have resulted in measurable animosity on par with racial animosity
\citep{Iyengar2015}.

Notably, partisanship appears to be influencing decision making in a
variety of otherwise apolitical contexts. For example, partisanship
appears to influence with whom individuals form romantic relationships
\citep{Huber2017, Iyengar2018}, as well as with whom individuals would
prefer to work \citep{McConnell2018}. Partisanship may even affect where
people choose to live \citep{Bishop2009, TamCho2013}. The underlying
mechanisms at work allowing partisanship to become so influential are
often described as either \emph{partisan homophily} or \emph{partisan
discrimination}. The former accounts for contexts in which individuals
wish to join with and support other in-party members, while the latter
describes contexts in which individuals wish to punish and distance
themselves from out-party members.

The acceptance of partisanship's wide-ranging influence has since led to
the emergence of popular narratives of partisan cultural divergence.
Claims of this type are generally structured around the presentation of
aggregate level preference or choice data that appear to show that
partisans prefer different television programs \citep{Katz2016}, books
\citep{Piper2016a}, and clothes \citep{WSJPartisanClothes}, to name just
a few examples. Complementing these popular accounts in the mass media
are academic studies showing that such aggregate group-level differences
may actually exist in regards to our cultural and entertainment tastes
\citep{Shi2017, Bertrand2018, Mutz2018}. These results are particularly
credible because social-psychology and consumer-behavior research has
found that individuals' consumption decisions are influenced by the need
to both signal in-group affiliation, while also signaling non-membership
in opposed groups \citep{Berger2008, Chan2012, Brewer1991}.

This alignment of cultural taste with partisanship is a particularly
important aspect of contemporary partisanship because of how such a
relationship facilitates further affective polarization. Partisans now
broadly share a set of cultural stereotypes about members of the two
parties and use these stereotypes to infer the partisan leanings of
strangers
\citep{Deichert2018Thesis, HiaeshutterRice2019, Hetherington2018}. In
turn, these inferences enable individuals to employ partisan biases in
interpersonal situations that would otherwise be devoid of partisan
conflict \citep{Deichert2018Thesis}. As a result, these situations
exacerbate partisan tensions and worsen the conflictual nature of
contemporary political life.

However, the evidence underlying the notion of partisanship as a
decision-making heuristic is still widely debated. Partisanship has been
shown to be a factor in hypothetical evaluations of locations to move
to, but is often superseded by other concerns when individuals actually
move \citep{Nall2017}. Even in the case of news-media consumption, a
behavior frequently associated with partisanship
\citep{Iyengar2009, Stroud2008, Garrett2009b}, new evidence suggests
that partisan differences are relatively small
\citep{Guess2018, Gentzkow2011, Eady2019, Barbera2015c}. If partisanship
is not as influential in everyday life as many assume, then partisan
cultural stereotypes may be grounded in misperceptions of the other
side, which are common \citep{Ahler2018}.

As such, this scholarly conflict over the influence of partisanship
leaves open the question: \emph{does one's partisanship or political
orientation influence their cultural taste}? In this study, I answer the
question by analyzing an audience of film viewers who use the Internet
Movie Database (IMDB) to keep track of their film ratings. Of course, if
the answer to this question is yes, how would we know? In this case, if
partisanship or political orienation is influencing audience members
cultural taste, we would expect them to share film preferences -- or
membership in what Netflix calls \emph{taste communities} -- with others
with similar political leanings (Hypothesis 1). However, the alternative
hypothesis, here, is that political orientation has no empirical effect
on the formation of taste communities (Hypothesis 2). Additionally, if
ideology is related to cultural taste we should expect those at the
ideological extremes to belong to the most politically homogenous taste
communities (Hypothesis 3). Alternatively, if ideology is not related to
cultural taste, then we should find that there is no relationship
between the political diversity of one's taste community and the
extremity of one's political orientation (Hypothesis 4).

\hypertarget{data-and-method}{%
\section{Data and Method}\label{data-and-method}}

The data analyzed in this study were originally collected as part of the
Movie Tweetings project \citep{MovieTweetings}. The Movie Tweetings
project began as a way to improve the available datasets for research on
recommendation systems. The project involves the perpetual scraping of
tweets generated through IMDB when users decide to share the rating they
have given to a film.

When I downloaded the Movie Tweetings database, it contained 819,845
ratings from 60,658 users. These ratings covered 37,051 movies. The
ratings themselves are recorded directly from the IMDB 10-point scale.
Importantly, though, the ratings also include the Twitter id for each
reviewer. This data allows us to associate other profile information
with each set of reviews. For this study, I scraped the list of accounts
being followed by each reviewer with at least 100 scraped ratings. This
threshold left only 1,642 users for whom I collected data. I then fed
the lists of accounts being followed into Barbera's method for
estimating political ideology from the set of elites an account follows
on Twitter \citep{Barbera2015c}. 579 of the users had insufficient data
to generate an ideology estimate and were dropped from further analysis.
This left 1,063 users in the final set.

This list of users was then applied as a filter to the ratings data.
Additionally, the ratings data were manipulated to create a new binary
indicator variable, equal to one whenever a rating was greater than 5 on
IMDB's 10 point scale. This variable captured instances in which users
signaled a positive reaction to a film and was used as another filter so
that ratings were only preserved when this indicator was equal to one,
leaving 209,873 ratings.

These ratings are inherently relational, mapping links between users and
films as a bipartite network. In this case, the network edges represent
instances in which users, the first class of nodes, reacted positively
to films, the second class of nodes. From this bipartite network of
users and films, I performed a further manipulation by generating the
one-mode projection representing the user-to-user network. In the
user-to-user network, each node represents one of the 1,063 users
allowed through our earlier filter and the directed edges connecting
them mark instances in which two users both reacted positively to the
same film. These edges have a weight equal to the number of films the
given pair of reviewers both rated positively. This network generating
process is visualized in \textbf{Figure 1}.

As a final threshold, I applied a disparity filter to this weighted
directed network of users. The disparity filter \citep{Serano2009} is a
statistical test for each edge in a weighted network, given that some
shared preferences could happen by chance alone, and not shared taste,
as the number of ratings one provides increases. This statistical test
differs from some other network thresholding techniques
\citep{Mukerjee2018} in that it establishes the null model the observed
results are compared against for each ego network, not every linked pair
of nodes. This difference is crucial as it determines significant edges
in a network based on information from all of the edges associated with
a given node. The null model in this technique assumes that edge weights
are drawn from a uniform distribution across the interval {[}0, 1{]}, so
that each edge has the same weight. Edges whose normalized weight (so
that the weights fall on the {[}0, 1{]} interval) is above a critical
value determined by the threshold set for significance. In this study,
that value, \(p\), is set to 0.05, the traditional statistical threshold
for significance. This means that each tie that is preserved occurs with
low probability if edge weights are assigned randomly. Applying the
disparity filter, reduced the number of edges to 18,160, down from
549,309 edges in the original network.

\begin{figure}[t]

{\centering \includegraphics[width=1\linewidth,]{../figures/schematic} 

}

\caption{A schematic of the network generating process. }\label{fig:fig1}
\end{figure}

The decision to analyze the user-to-user network marks an important
point of departure from prior network-based studies of audience behavior
and fragmentation. These earlier studies, due to limitations imposed by
data sources, were forced into studying the outlet-to-outlet or
product-to-product network that is the other possible one-mode
projection from this type of bipartite network
\citep{Webster2012, Mukerjee2018}. These studies have focused on
audience fragmentation, but have been forced to study such behavior
through the structure of the outlet-to-outlet or product-to-product
networks projected from the user-media bipartite network. As a result,
any analyses of audience characteristics have been done with
product-level aggregates, increasing the uncertainty of their results.

Because I am analyzing the user-to-user network, I can conduct my
analyses without having to generate average values for node-level
features, which can introduce additional levels of uncertainty. Instead,
I am able to assess the relationship between network structure and the
specific estimate of political ideology returned from Barbera's method
for each user in the network. In particular, I focus my analysis on each
user's ego network. An ego network is the subgraph of a network
generated by a given node and all other nodes with whom it shares an
edge. The central node in an ego network is commonly referred to as
\emph{ego}. Each ego network can be described by its average ideology,
operationalized here as the average Twitter ideology score of ego's
neighbors, weighted by the associated weight of the edge running from
ego to each neighbor, as well as its diversity of Twitter ideology
scores, operationalized as the variance in the ideology scores of ego's
neighbors, weighted again by the weight of the edge from ego to each
neighbor. This process is visualized in \textbf{Figure 2}. To test the
hypothesis that political orientation is associated with cultural taste,
I visualize and model the relationship between these two measures and
the estimated ideology score of each ego.

\begin{figure}[t]

{\centering \includegraphics[width=1\linewidth,]{../figures/method_schematic} 

}

\caption{A schematic of the network analytic method. }\label{fig:fig2}
\end{figure}

\hypertarget{results}{%
\section{Results}\label{results}}

The initial bipartite network made up of 819,845 edges between 60,658
IMDB users and 37,051 films was first thresholded so as to keep only the
network of users with at least 100 captured reviews. Furthermore, to
take taste into account, only reviews greater than a 5 were preserved.
This left 209,873 edges in the bipartite network between 1,063 users and
20,507 films. The initial one-mode projection of the user-to-user
network consisted of 549,309 undirected edges between the 1,063 users.

To further preserve only those edges that we would not expect to occur
from random chance, I performed backbone extraction on the network by
applying a disparity filter \citep{Serrano2009, Majo2019}. This filter
applied a statistical test to each tie in each ego network contained
within the larger whole. Those edges that could have occurred by random
chance were subsequently removed. This process left 18,160 edges between
the 1,063 users.

We can interpret the edges in this network as marking instances in which
two users, represented as linked nodes, both positively rated at least
one of the same films. The weights given to edges, assuming they link
User A and User B, were weighted to indicate the number of the movies
preferred by User A which were also preferred by User B. These weights
range between 5 and 499, with a median of 55 and a mean of 64.81. That
these edges represent connections based on actual taste preferences, we
can consider the network structure to be a map of shared cultural taste
and taste communities.

Additionally, each user node in this network also has three measured
properties: the number of positive ratings they generated, the number of
neighbors they were connected to, and their one-dimensional ideology
score estimated based on the elites they follow on Twitter, with low
values indicating more liberal or Democratic positions and higher scores
indicating more conservative or Republican positions. The distributions
for the number of positive ratings and number of neighbors are
predicatably heavy-tailed. The number of positive ratings ranges from 33
to 1,968, with a median of 148 and a mean of 197.4. The number of
neighbors a node has ranges from 2 to 1,045, with a median of 17 and a
mean of 34.17.

However, these 1,063 users have a more symmetric and representive
distribution of political leanings, based on the one-dimensional score
generated from Barbera's method of analyzing the elite accounts each
user follows on Twitter \citep{Barbera2015c}. The distribution of
one-dimensional ideology scores is wide ranging, running from -2.37 to
2.41, with a median (0.19) and mode (.38) to the right of zero , but a
mean value very nearly zero (0.083). This distribution suggests that
this sample audience should be sufficient for identifying links between
partisanship and cultural taste because it incorporates individuals
across a wide swath of the American ideological landscape. There are
enough conservatives in the group that if they are to be connected to
anyone, they do not have to be connected to liberals and vice versa.

To test whether ideology is associated with this structure, I extracted
and analyzed the ego-network for each user in the network. The ego
network is the subgraph consisting of a given node and each node
connected directly to it and is analogous to a small taste community. I
then took the mean of the neighbor's one-direction ideology scores,
weighting by the weight of the edge running from ego to each neighbor.
If this average is close to the ideology score inferred for the ego,
then that would suggest that ideology and cultural taste are linked,
lending credence to Hypothesis 1 and undermining Hypothesis 2.

\begin{figure}[t]
\includegraphics{manuscript_files/figure-latex/fig5-1} \caption{A. Relationship between user ideology score and the weighted average of their neighbors. Curve generated by local regression (loess) appears in grey. B. Relationship between us er ideology score and the weighted variance in ideology score of their neighbors. Curve generated by local regression (loess) appears in grey.}\label{fig:fig5}
\end{figure}

\begin{table}[t] \centering 
  \caption{Regression results modeling ego network average ideology on user ideology.} 
  \label{} 
\begin{tabular}{@{\extracolsep{5pt}}lccc} 
\\[-1.8ex]\hline 
\hline \\[-1.8ex] 
 & \multicolumn{3}{c}{\textit{Dependent variable:}} \\ 
\cline{2-4} 
\\[-1.8ex] & \multicolumn{3}{c}{Mean Ego Network Ideology} \\ 
\\[-1.8ex] & (1) & (2) & (3)\\ 
\hline \\[-1.8ex] 
 User Ideology & 0.001 & 0.0003 & $-$0.001 \\ 
  & (0.002) & (0.002) & (0.003) \\ 
  & & & \\ 
 User Ideology$^{2}$ &  & $-$0.003$^{**}$ & $-$0.003$^{*}$ \\ 
  &  & (0.001) & (0.001) \\ 
  & & & \\ 
 User Ideology$^{3}$ &  &  & 0.001 \\ 
  &  &  & (0.001) \\ 
  & & & \\ 
 Constant & 0.065$^{***}$ & 0.067$^{***}$ & 0.067$^{***}$ \\ 
  & (0.001) & (0.002) & (0.002) \\ 
  & & & \\ 
\hline \\[-1.8ex] 
Observations & 2,150 & 2,150 & 2,150 \\ 
R$^{2}$ & 0.0002 & 0.002 & 0.002 \\ 
Adjusted R$^{2}$ & $-$0.0003 & 0.001 & 0.001 \\ 
Residual Std. Error & 0.062 (df = 2148) & 0.062 (df = 2147) & 0.062 (df = 2146) \\ 
F Statistic & 0.388 (df = 1; 2148) & 2.174 (df = 2; 2147) & 1.561 (df = 3; 2146) \\ 
\hline 
\hline \\[-1.8ex] 
\textit{Note:}  & \multicolumn{3}{r}{$^{*}$p$<$0.1; $^{**}$p$<$0.05; $^{***}$p$<$0.01} \\ 
\end{tabular} 
\end{table}

\begin{table}[t] \centering 
  \caption{Regression results modeling ego network variance on user ideology.} 
  \label{} 
\begin{tabular}{@{\extracolsep{5pt}}lcc} 
\\[-1.8ex]\hline 
\hline \\[-1.8ex] 
 & \multicolumn{2}{c}{\textit{Dependent variable:}} \\ 
\cline{2-3} 
\\[-1.8ex] & \multicolumn{2}{c}{Var. of Ego Network Ideology} \\ 
\\[-1.8ex] & (1) & (2)\\ 
\hline \\[-1.8ex] 
 User Ideology & 0.001 & 0.001 \\ 
  & (0.002) & (0.002) \\ 
  & & \\ 
 User Ideology$^{2}$ &  & 0.001 \\ 
  &  & (0.002) \\ 
  & & \\ 
 Constant & 0.833$^{***}$ & 0.832$^{***}$ \\ 
  & (0.002) & (0.002) \\ 
  & & \\ 
\hline \\[-1.8ex] 
Observations & 2,150 & 2,150 \\ 
R$^{2}$ & 0.0001 & 0.0002 \\ 
Adjusted R$^{2}$ & $-$0.0004 & $-$0.001 \\ 
Residual Std. Error & 0.073 (df = 2148) & 0.073 (df = 2147) \\ 
F Statistic & 0.155 (df = 1; 2148) & 0.253 (df = 2; 2147) \\ 
\hline 
\hline \\[-1.8ex] 
\textit{Note:}  & \multicolumn{2}{r}{$^{*}$p$<$0.1; $^{**}$p$<$0.05; $^{***}$p$<$0.01} \\ 
\end{tabular} 
\end{table}

However, I find the opposite. As we can see in Panel A of \textbf{Figure
5}, the relationship between the inferred ideology each user and the
average ideology of their neighbors in the network is basically zero;
the overlaid loess curve shows very little movement across the range of
values. The network assortativity, a correlation-like network measure,
in regards to ideology confirms the result: it is only 0.034 on a scale
ranging from -1 to 1. Furthermore, we can see in Column 1 of
\textbf{Table 1} that a linear relationship between a user's political
orientation and that of their neighbors, which is predicted by
Hypothesis 1, is not present, undermining the theory and lending
credibility to Hypothesis 2. Columns 2 and 3 of \textbf{Table 1} further
show that even if we ignore the visualization and try to force a
quadratic or cubic relationship between the ego network average and the
associated ego's ideology score, there is still no statistically
significant relationship.

Additionally, as stated in Hypothesis 3, if ideology is related to
cultural taste we should expect those at the ideological extremes to
belong to the most politically homogenous taste communities. That is the
relationship between user ideology and the variance in the ideology
scores in each ego network should be concave. As a test of this
hypothesis, I calculated the variance in the one-dimensional ideology
scores for each user's neighbors, again weighted by the weight of the
edge from the user to each neighbor. The relationship between user
ideology and the variance in the ideology of their neighbors is plotted
in Panel B of \textbf{Figure 5}. We can see from the overlayed loess
curve that the relationship is, if anything other than flat, convex,
with higher values at the tails than at the center. To confirm this
observation, I regressed the variance in the neighbors' ideology scores
on models including user ideology scores as just a linear term and as
both the linear and squared terms, with full reults reported in
\textbf{Table 2}. I find that in the second model, the quadratic term is
positive (\(\beta = 0.023\)), but not statistically significant. This
lack of significant effects, indicates that there is no relationship
between one's own political orientation and the homogeneity in the
political orientation of those with whom one shares film preferences,
violating our expectations for what should happen if ideology and
cultural taste are related, as stated in Hypothesis 3. Therefore, it
appears that Hypothesis 4 is the more credible of the pair.

\hypertarget{discussion}{%
\section{Discussion}\label{discussion}}

In this study, I set out to provide a strong individual-level empirical
test of the claim that American partisans are experiencing cultural
divergence. These claims, amplified by the popular press and supported
by academics presenting aggregated results at the group level. Here,
I've paired IMDB film ratings from 1,063 users with data about which
elites these individuals follow on Twitter, transformed into a
one-dimensional score correlated with political ideology and political
orientation. I have used these paired data to construct a network
linking users together when they both provide a positive review of the
same film. By extracting the local neighborhood of each user in the
network, I am able to compare the estimated ideology of each user with
those who most share their taste profile. This approach also allows me
to test whether the diversity of political orientations among one's
closest taste neighbors varried less as one's political orientation
grows more extreme. I find no meaningful relationships in either case,
undermining the argument that political orientation is associated with
cultural taste.

These results fit nicely into the recent literature arguing against
partisanship's influence in other apolitical settings, such as making
decisions about where to live \citep{Nall2017}. While this study does
not incorporate any tests of any specific mechanisms, we can easily
imagine that cultural taste functions similarly to taste in where to
live: it is likely subject to many competing interests or desires. These
desires or interests may occasionally align with those related to group
identification and differentiation detailed in Optimal Distinctiveness
Theory (ODT) \citep{Brewer1991, Berger2007, Berger2008}, but may not be
activated unless group-identification is primed. We know that in
specific political contexts, social settings play an important role in
how partisanship influences reasoning \citep{Klar2014}. Further research
on the influence of partisanship in apolitical settings should focus on
whether or not partisanship's influence on cultural taste is also
dependent on social context.

Additionally, further research could benefit from attempting to better
map the many influences on cultural taste left unmeasured in this study.
Properly taking these factors into account may provide a pathway for
partisanship to exert an influence on taste and, in turn, begin to make
precise causal arguments possible. Yet, such a mediated or moderated
causal path diverges from the spirit of the bold claims made in the
popular press about the substantial fissure developing between Democrats
and Republicans. As such, the analyses and results presented in this
study are appropriate for testing and refuting this strong claim. The
result that individuals are located within taste communities that are
politically diverse, regardless of how extreme one's own political
orienatation appears to be, suggests that political homogeneity is not a
feature of taste communities.

\newpage




\newpage
\singlespacing 
\renewcommand\refname{References}
\bibliography{../../references/fischerbib.bib}

\end{document}
