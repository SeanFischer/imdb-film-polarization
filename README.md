# Politically Diverse Taste Communities

This repository contains the replication materials for the working paper "Politically Diverse Taste Communities".

To replicate all analyses performed in the paper, clone the project repository, and open scripts in R.

Minimal datasets are provided to test the analyze_network.R and ego_networks.R scripts, as well as the included code chunks in manuscript.Rmd.

## Main Text Figures and Tables


## Appendix Text and Figures
### Positive Reviews Full Specifcations

### Negative Reviews Network w/ Disparity Filter

### Negative Reivews Network w/out Disparity Filter
To conduct the analyses related to the negative-review networks without the use of the disparity filter run the following scripts in order:

1. build_network_3.R
2. analyze_network_3.R (Runs ego_networks_3.R as part of the script)