library(dplyr)
library(Matrix)
library(igraph)
library(ggraph)
library(graphlayouts)

# load movie data
movies <- read.delim("data/movieTweetings/movies.dat",
                     sep = ":", header = FALSE,
                     colClasses = c("character", "character",
                                    "character", "character",
                                    "character"))

# rename movie data columns and select them as needed
movies <-
  movies %>%
  mutate(movieID = V1,
         movie_title = V3,
         genre = V5,
         movieID = paste0("tt", movieID)) %>%
  select(movieID, movie_title, genre)

# load ratings data
ratings <- read.delim("data/movieTweetings/ratings.dat",
                      sep = ":", header = FALSE,
                      colClasses = c("character", "character",
                                     "character", "character",
                                     "numeric", "character",
                                     "character"))

# rename and select columns
ratings <-
  ratings %>%
  mutate(userID = V1,
         movieID = V3,
         rating = V5,
         rating_timestamp = V7,
         movieID = paste0("tt", movieID)) %>%
  select(userID, movieID, rating, rating_timestamp)

# calculate number of ratings per user
user_ratings <-
  ratings %>%
  group_by(userID) %>%
  summarise(N = n())

# filter out users with less than 25 ratings
# user_ratings_filt <-
#   user_ratings %>%
#   filter(N >= 25)

load("data/users_filt_2.RData")
users_filt_2 <- users_filt
load("data/users_filt_3.RData")

load("data/users_filt.RData")

users_filt <-
  users_filt %>%
  bind_rows(users_filt_2) %>%
  bind_rows(users_filt_3) %>%
  filter(!is.na(twitter_ideology),
         twitter_ideology != -Inf,
         twitter_ideology != Inf) %>%
  mutate(userID = as.character(userID)) %>%
  arrange(userID)

# filter out ratings from users not in the filtered user list and consider
# only positive ratings
ratings_filt <-
  ratings %>%
  filter(userID %in% users_filt$userID) %>%
  mutate(ratings_2 = case_when(rating > 5 ~ 1,
                               rating < 5 ~ -1,
                               rating == 5 ~ 0)) %>% # positive === rating > 5
  filter(ratings_2 == 1)

user_ratings_2 <-
  ratings_filt %>%
  group_by(userID) %>%
  summarise(N = n())

# create a matrix representing the bipartite network
A <- spMatrix(nrow = length(unique(ratings_filt$userID)),
              ncol = length(unique(ratings_filt$movieID)),
              i = as.numeric(factor(ratings_filt$userID)),
              j = as.numeric(factor(ratings_filt$movieID)),
              x = rep(1, length(as.numeric(ratings_filt$userID))) )
row.names(A) <- levels(factor(ratings_filt$userID))
colnames(A) <- levels(factor(ratings_filt$movieID))

# create a matrix representing the user-to-user one-mode projection
Arow <- tcrossprod(A)

# scale weights by number of ratings per user
# Arow <- Arow / diag(Arow)

# remove the bipartite network matrix
rm(A)

# create graph
graph <- graph_from_adjacency_matrix(Arow, weighted = TRUE, mode = "undirected", 
                                     diag = FALSE)

# apply disparity filter to extract the backbone of the network
graph_bb <- skynet::disparity_filter(graph, alpha = 0.05)

# remove the original graph and its associated matrix
rm(graph)
rm(Arow)


users_filt <-
  users_filt %>%
  filter(userID %in% V(graph_bb)$name)

V(graph_bb)$twitter_ideology <- users_filt$twitter_ideology

# plot the network after thresholding

# set.seed(101)
# bb <- layout_as_backbone(graph_bb, keep=0.4)
# E(g)$col <- F
# E(g)$col[bb$backbone] <- T

# ggraph(simplify(graph_bb, remove.loops = TRUE, remove.multiple = FALSE),
#        layout = "fr") +
#   geom_edge_arc(color = "grey60", width = 0.1) +
#   geom_node_point(aes(color = twitter_ideology)) +
#   scale_color_gradient("Ideology Score", low = "#5522ee", high = "#d53737",
#                        limits = c(-2.5, 2.5)) +
#   theme_bw() +
#   theme(panel.grid.minor = element_blank(),
#         panel.grid = element_blank(),
#         legend.position = "bottom",
#         legend.direction = "horizontal",
#         legend.text = element_text(vjust = 0.5),
#         axis.text = element_blank(),
#         axis.ticks = element_blank(),
#         axis.title = element_blank()) +
#   guides(color = guide_colourbar(title.position = "top", title.hjust = 0.5))
